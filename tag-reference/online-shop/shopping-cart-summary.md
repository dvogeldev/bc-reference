## Shopping Cart Layout

### Tags

Tag | Description
-------------- | -------------
`{tag_cartUrl}` | Renders the URL to the cart summary page, which can be either OrderRetrievev2.aspx or OrderRetrieve.aspx (the old, legacy v1 version)
`{tag_itemCount}` | Renders the number of the items in the cart or 0 if none.
`{tag_totalAmount}` | Renders the total money amount of the items (with two decimals) or 0.00 if the items don't have prices or there are no items in the cart
`{tag_currency}` | Renders the currency of the current country setting, which is either the site country or the country of the order (if set)
`{tag_totalAmountWithCurrency}` | Renders both currency and total amount (this is only for the sake of convenience, you might want to use it instead of {tag_currency}{tag_totalAmount})
`{tag_isEmpty}` | Renders 0 or 1 (for when the cart is empty - used to display a different message when there are no items)
`{tag_countryCode}` | Renders the country code (of the site or of the order, if set)
