## API endpoints

Business Catalyst offers a wide range of API endpoints that allow developers to extend the core functionality of the platform. 

The APIs are split across two protocols, with the majority being RESTful:

### REST APIs

* Categories
* File System
* Page Templates
* Roles
* Sites
* System
* Web App items
* Web Apps

### SOAP APIs

* CRM
* eCommerce


